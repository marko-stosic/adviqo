<?php

namespace App\ImageVariations;

/**
 * Class UserImageVariations
 * @package App\ImageVariations
 */
class ProfileImageVariations extends ImageVariationsBase
{
    /**
     * @return string[]
     */
    public static function getSizes()
    {
        return [
            'xl' => '800x800',
//            'lg' => '500x500',
//            'md' => '300x300',
//            'sm' => '150x150',
//            'xs' => '48x48'
        ];
    }
}
