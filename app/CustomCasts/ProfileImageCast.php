<?php

namespace App\CustomCasts;

use App\ImageVariations\ProfileImageVariations;

/**
 * Class UserImageCast
 * @package App\CustomCasts
 */
class ProfileImageCast extends AbstractImageCast
{
    public static function storageDir()
    {
        return 'uploads/advisor';
    }

    public static function imageVariationsClass()
    {
        return ProfileImageVariations::class;
    }
}
