<?php

namespace App\CustomCasts;

use Exception;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use InvalidArgumentException;
use Vkovic\LaravelCustomCasts\CustomCastBase;

/**
 * Class AbstractImageCast
 * @package App\CustomCasts
 */
abstract class AbstractImageCast extends CustomCastBase
{
    /**
     * Callback
     *
     * @var
     */
    protected $storeImagesCallback;

    /**
     * Get storage dir (relative to app/storage)
     *
     * @return string
     */
    abstract static function storageDir();

    /**
     * Get full qualified image variations class name
     *
     * @return string
     */
    abstract static function imageVariationsClass();

    /**
     * Image sizes e.g.: ['lg' => '1024x768', 'xl' => '1280x1024]
     *
     * @return array
     */
    public static function imageSizes()
    {
        return static::imageVariationsClass()::getSizes();
    }

    public function castAttribute($value)
    {
        $placeholder = 'uploads/placeholders/placeholder.png';

        $image = env('FORCE_PLACEHOLDERS') === true
            ? $placeholder
            : ($value ?? $placeholder);

        $imageVariationsClass = static::imageVariationsClass();

        return new $imageVariationsClass($image);
    }

    /**
     * @param $value
     *
     * @return string String to be inserted into database
     *
     * @throws Exception
     */
    public function setAttribute($value)
    {
        // New value to be saved in corresponding db field
        $newValue = null;

        // Handle base64 image string (uploading via Backpack)
        if (Str::startsWith($value, 'data:image') || $value instanceof UploadedFile) {
            $extension = $value instanceof UploadedFile
                ? $value->extension()
                : $this->getBase64fileExtension($value);

            $filename = Str::random(12);

            // Make image object from base64 string
            $image = Image::make($value);
            $originalRatio = $image->getWidth()/$image->getHeight();
            $newValue = static::storageDir() . '/' . $this->attribute . '-' . $filename . '.' . $extension;

            // Make sure images are saved after model is saved,
            // not here (when attribute is set)
            $this->storeImagesCallback = function () use ($image, $filename, $extension, $newValue, $originalRatio) {
                $imageQuality = env('UPLOAD_IMAGE_QUALITY', 100);
                $originalImage = clone $image;

                // Store original
                $image->save(storage_path('app/' . $newValue), $imageQuality);

                // Store other image sizes
                foreach (static::imageSizes() as $imageSize) {
                    list($width, $height) = explode('x', $imageSize);
                    if ($originalRatio > 0) {
                        $height = round($height / $originalRatio);
                    } else {
                        $width = round($width * $originalRatio);
                    }
                    $optimalImageQuality = $this->normalizeQuality($width, $height, $imageQuality);

                    // Variant image name and path
                    $variantName = $this->attribute . '-' . $filename . '-' . $imageSize . '.' . $extension;
                    $variantRelPath = static::storageDir() . '/' . $variantName;

                    // Save image with defined quality
                    $originalImageClone = (clone $originalImage);
                    $originalImageClone->fit($width, $height)
                        ->save(storage_path('app/' . $variantRelPath), $optimalImageQuality);
                }

                $image = null;
                $originalImage = null;
                $originalImageClone = null;
            };
        } else if ($this->model->{$this->attribute} === $value) {
            $newValue = $value;
        } else if (Str::startsWith($value, 'http')) {
            $newValue = $this->model->{$this->attribute}->source();
        } else if (!is_null($newValue)) {
            throw new InvalidArgumentException('Image needs to be base64 encoded string or uploaded file');
        }

        return $newValue;
    }

    /**
     * Get extension from base64 string
     *
     * @param $base64string
     *
     * @return string
     * @throws Exception
     *
     */
    protected function getBase64fileExtension($base64string)
    {
        $start = strpos($base64string, '/') + 1;
        $end = strpos($base64string, ';');

        if ($start === false || $end === false) {
            throw new Exception('Can`t get extension from base64 encoded string');
        }

        return substr($base64string, $start, $end - $start);
    }

    /**
     * Optimize image quality.
     * This is needed because if we set quality to e.g 75
     * small images (less than 300x300px) quality will be bad
     *
     * @param int $width
     * @param int $height
     * @param int $baseQuality
     *
     * @return int
     *
     */
    protected function normalizeQuality(int $width, int $height, int $baseQuality)
    {
        $minResolution = 300 * 300;

        if ($width * $height <= $minResolution) {
            return 100;
        }

        return $baseQuality;
    }

    /**
     * React to created event
     *
     * Handle initial image saving
     */
    public function created()
    {
        if (is_callable($this->storeImagesCallback)) {
            ($this->storeImagesCallback)();

            // It appears that this callback somehow does not get unset properly,
            // so, we'll fo it manually.
            // It takes lots of memory, especially while seeding.
            // This cold be due to cloning in callback or callback itself ...
            unset($this->storeImagesCallback);
        }
    }

    /**
     * React to updating event
     *
     * Handle image replacing here, because we can check if corresponding
     * model field is dirty and update accordingly
     */
    public function updating()
    {
        // Check if image field is changed and if callback is defined
        if ($this->model->isDirty($this->attribute)) {
            // Delete old file
            $this->deleteImages();

            // Callback to save file on model updating
            if (is_callable($this->storeImagesCallback)) {
                ($this->storeImagesCallback)();
            }
        }
    }

    /**
     * Reach to deleted event
     *
     * Handle image deleting
     */
    public function deleted()
    {
        // Delete related images when model is deleted
        $this->deleteImages();
    }

    /**
     * Delete original images as well as other variations (dimensions)
     */
    protected function deleteImages()
    {
        $sourceImagePath = $this->model->getOriginal($this->attribute);

        if ($sourceImagePath && File::exists(storage_path('app/' . $sourceImagePath))) {
            $pathInfo = pathinfo($sourceImagePath);
            $dir = $pathInfo['dirname'];
            $filename = $pathInfo['filename'];

            $pattern = storage_path('app/' . $dir) . '/' . $filename . '*';

            File::delete(File::glob($pattern));
        }
    }

    public function __destruct()
    {
        $this->storeImagesCallback = null;
    }
}
