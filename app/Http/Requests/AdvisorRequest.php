<?php

namespace App\Http\Requests;

use App\Models\Language;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AdvisorRequest
 * @package App\Http\Requests
 */
class AdvisorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:80'],
            'description' => 'nullable',
            'availability' => 'nullable',
            'price_per_minute' => ['required', 'numeric'],
            'language_codes' => ['required', 'array'],
            'language_codes.*' => ['in:' . implode(',', Language::pluck('short_code')->toArray())],
            'profile_image' => ['nullable', 'starts_with:data:image']
        ];
    }
}
