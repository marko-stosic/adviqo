<?php

namespace App\Http\Controllers\Api;

use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse as Response;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Routing\Redirector;

/**
 * Class ResourceController
 * @package App\Http\Controllers\Api
 */
abstract class ResourceController
{
    protected Model $model;

    protected FormRequest|string $formRequest;

    protected array $filterableColumns;

    protected array $sortableColumns;

    abstract protected function getModel(): Model;

    abstract protected function getResource(?Model $model = null): JsonResource;

    abstract protected function getFilterableColumns(): array;

    abstract protected function getSortableColumns(): array;

    abstract protected function getFormRequest(): string;

    /**
     * ResourceController constructor.
     */
    public function __construct()
    {
        $this->model = $this->getModel();
        $this->filterableColumns = $this->getFilterableColumns();
        $this->sortableColumns = $this->getSortableColumns();
        $this->formRequest = $this->getFormRequest();
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        $query = $this->model::query();

        $query = $this->getQueryWithRelationships($query, $request);
        $query = $this->getFilteredQuery($query, $request);
        $query = $this->getSortedQuery($query, $request);

        return $this->getResource()::collection($query->get())->response();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function store(Request $request): Response
    {
        $formRequest = $this->initRequest($request);

        /** @var Model $model */
        $model = new $this->model;
        $model->fill($formRequest->validated());
        $model->save();

        $relations = $formRequest->query('include') ? explode(',', $formRequest->query('include')) : [];

        return $this->getResource($model->load($relations))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int     $id
     * @param  Request $request
     *
     * @return Response
     */
    public function show(int $id, Request $request): Response
    {
        $model = $this->model->query()->findOrFail($id);

        $relations = $request->query('include') ? explode(',', $request->query('include')) : [];

        return $this->getResource($model->load($relations))->response();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     */
    public function update(Request $request, int $id): Response
    {
        $model = $this->model->query()->findOrFail($id);

        $formRequest = $this->initRequest($request);

        $model->update($formRequest->validated());

        $relations = $formRequest->query('include') ? explode(',', $formRequest->query('include')) : [];

        return $this->getResource($model->load($relations))->response();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int      $id
     * @param Response $response
     *
     * @return Response
     * @throws Exception
     */
    public function destroy(int $id, Response $response): Response
    {
        $model = $this->model->query()->findOrFail($id);
        $model->delete();

        return $response->setStatusCode(Response::HTTP_NO_CONTENT);
    }

    /**
     * Get query with filtered result.
     *
     * @param  Builder $query
     * @param  Request $request
     *
     * @return Builder
     */
    private function getFilteredQuery(Builder $query, Request $request): Builder
    {
        $params = $request->query();
        $searchParams = array_intersect_key($params, $this->filterableColumns);

        if (!empty($searchParams)) {
            // @Todo implement logic to check all possible operators (=, =-, =~, =<, =>, =*...*, ...)
            foreach ($searchParams as $param => $value) {
                if ($this->filterableColumns[$param]['type'] === 'column') {
                    $query = $query->where($this->filterableColumns[$param]['attribute'], $value);
                }
                if ($this->filterableColumns[$param]['type'] === 'relationship') {
                    $query = $query->whereHas(
                        $this->filterableColumns[$param]['name'],
                        function (Builder $subQuery) use ($param, $value) {
                            return $subQuery->where($this->filterableColumns[$param]['attribute'], $value);
                        }
                    );
                }
            }
        }

        return $query;
    }

    /**
     * Get query with sorted results.
     *
     * @param  Builder $query
     * @param  Request $request
     *
     * @return Builder
     */
    private function getSortedQuery(Builder $query, Request $request): Builder
    {
        $sortParams = explode(',', $request->query('sort'));
        $sortableColumns = $this->sortableColumns;
        foreach ($sortParams as $sortParam) {
            $direction = 'ASC';
            $firstChar = substr($sortParam, 0, 1);
            if (in_array($firstChar, ['-', '+'])) {
                $sortParam = substr($sortParam, 1);
                $direction = $firstChar === '+' ? 'ASC' : 'DESC';
            }
            if (!array_key_exists($sortParam, $sortableColumns)) {
                continue;
            }
            $query = $query->orderBy($sortableColumns[$sortParam], $direction);
        }

        return $query;
    }

    /**
     * Get query with loaded relationships.
     *
     * @param  Builder $query
     * @param  Request $request
     *
     * @return Builder
     */
    private function getQueryWithRelationships(Builder $query, Request $request): Builder
    {
        $include = $request->query('include');
        if ($include) {
            $include = explode(',', $include);
            $query = $query->with($include);
        }

        return $query;
    }

    /**
     * @param Request $request
     *
     * @return FormRequest|Request
     */
    protected function initRequest(Request $request)
    {
        $formRequest = $this->formRequest::createFrom($request);
        $formRequest->setContainer(app())
            ->setRedirector(app(Redirector::class))
            ->validateResolved();

        return $formRequest;
    }
}
