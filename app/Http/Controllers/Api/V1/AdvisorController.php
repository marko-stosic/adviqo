<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\ResourceController;
use App\Http\Requests\AdvisorRequest;
use App\Http\Resources\AdvisorResource;
use App\Models\Advisor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class AdvisorController
 * @package App\Http\Controllers\Api\V1
 */
class AdvisorController extends ResourceController
{

    protected function getModel(): Model
    {
        return new Advisor;
    }

    protected function getResource(?Model $model = null): JsonResource
    {
        return new AdvisorResource($model ?? $this->model);
    }

    protected function getFilterableColumns(): array
    {
        return [
            'name' => [
                'type' => 'column',
                'attribute' => 'name',
            ],
            'language' => [
                'type' => 'relationship',
                'attribute' => 'short_code',
                'name' => 'languages'
            ],
        ];
    }

    protected function getSortableColumns(): array
    {
        return [
            'price' => 'price_per_minute',
            'availability' => 'availability'
        ];
    }

    protected function getFormRequest(): string
    {
        return AdvisorRequest::class;
    }
}
