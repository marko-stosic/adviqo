<?php

namespace App\Http\Resources;

use App\Models\Advisor;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class AdvisorResource
 *
 * @mixin Advisor
 *
 * @package App\Http\Resources
 */
class AdvisorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'availability' => $this->availability,
            'price_per_minute' => $this->price_per_minute,
            'languages' => $this->whenLoaded('languages', function () {
                return LanguageResource::collection($this->languages);
            }),
            'profile_image' => $this->getImageWithSizes('profile_image')
        ];
    }
}
