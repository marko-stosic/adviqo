<?php

namespace App\Models;

/**
 * Interface ImageInterface
 * @package App\Models
 */
interface ImageInterface
{
    /**
     * @return array
     */
    public function imageSizeMapping(): array;
}
