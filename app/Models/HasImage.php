<?php

namespace App\Models;

/**
 * Trait HasDifferentImageSizes
 * @package App\Models
 */
trait HasImage
{
    /**
     * @param string $attribute
     * @return array|null
     */
    public function getImageWithSizes(string $attribute)
    {
        $sizeMaping = $this->imageSizeMapping();
        $imageSizes = array_keys($sizeMaping[$attribute]);
        $data = [];

        foreach ($imageSizes as $imageSize) {
            /** @var Advisor $this */
            $imageVariations = $this->{$attribute};

            if ($imageVariations && !$imageVariations->isPlaceholder()) {
                $data[$imageSize] = asset($imageVariations->$imageSize());
            }
        }

        return empty($data) ? null : $data;
    }
}
