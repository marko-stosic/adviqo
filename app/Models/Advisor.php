<?php

namespace App\Models;

use App\CustomCasts\ProfileImageCast;
use App\ImageVariations\ProfileImageVariations;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Vkovic\LaravelCustomCasts\HasCustomCasts;

/**
 * Class Advisor
 *
 * @property int     $id
 * @property string  $name
 * @property string  $description
 * @property boolean $availability
 * @property float   $price_per_minute
 * @property array   $language_codes
 * @property ProfileImageCast $profile_image
 * @property-read Collection|Language[] $languages
 *
 * @mixin Builder
 *
 * @package App\Models
 */
class Advisor extends Model implements ImageInterface
{
    use HasFactory, HasImage, HasCustomCasts;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name', 'description', 'availability', 'price_per_minute', 'profile_image', 'language_codes'
    ];

    protected $casts = [
        'availability' => 'boolean',
        'profile_image' => ProfileImageCast::class
    ];

    protected $appends = [
        'language_codes'
    ];

    public function languages(): BelongsToMany
    {
        return $this->belongsToMany(Language::class, 'advisors_languages', 'advisor_id', 'language_id');
    }

    public function imageSizeMapping(): array
    {
        return [
            'profile_image' => ProfileImageVariations::getSizes()
        ];
    }

    public function setLanguageCodesAttribute($value)
    {
        $this->language_codes = $value;
    }

    protected static function boot()
    {
        parent::boot();

        static::saved(function (Advisor $advisor) {
            $advisor->languages()->sync(Language::whereIn('short_code', $advisor->language_codes)->pluck('id')->toArray());
        });

        static::deleting(function (Advisor $advisor) {
            $advisor->languages()->sync([]);
        });
    }
}
