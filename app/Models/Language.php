<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Language
 *
 * @property int    $id
 * @property string $name
 * @property string $short_code
 * @property string $code
 *
 * @mixin Builder
 *
 * @package App\Models
 */
class Language extends Model
{
    use HasFactory;
}
